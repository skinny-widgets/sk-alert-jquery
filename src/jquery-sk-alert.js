
import { SkAlertImpl }  from '../../sk-alert/src/impl/sk-alert-impl.js';

export class JquerySkAlert extends SkAlertImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'alert';
    }

    get closeBtnEl() {
        if (! this._closeBtnEl) {
            this._closeBtnEl = this.comp.el.querySelector('.sk-alert-close-icon');
        }
        return this._closeBtnEl;
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }
}
